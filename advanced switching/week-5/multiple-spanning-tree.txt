MST - Multiuple Spanning Tree

MST allows the network to have multiple Spanning Tree topologies since having one for every vlan can be difficult to configure
MST allows the engineers to creat multiple spanning tree protocols and apply them on multiple vlans as suited.
this concept is in fact extendable to 4096 vlans: vlan load balancing.

MST converges faste than PVRST+ and is backward compatible with 802.1D STP and 802.1w.

MST configuration:

Enable MST on switch:
	spanning-tree mode mst

Enter MST config sub mode:
	spanning-tree mst configuration

Display current MST config:
	show current

Name MST instances:
	name [name]

set the 16-bit MST revision number. it is not incremented automatically when you commit a new MST config.:
	revision [rev_number]

Map vlan to mst instance. 
	instance [instance_num] vlan [vlan_range] 

Display new MST config to be applied.
	show pending

apply and exit MST config submode:
	exit

assing root bridge for MST instance. This syntax makes the switch root primary or secondary (only active if primary fails.)
it sets primary priority to 24576 and secondary to 28672. 
	spanning-tree mst [instance_number] root primary | secondary
