NTFS stands for new technology file system.
it was originally developed for Windows NT to replace FAT.
supports up to 2^64 bytes of datea.
main features of it are:
	
	1.Recoverablity: since it uses a jouranling file system it can 
	recover from system failures.
	
	2.A security descriptor is associated with open files which efine its security attributes.
		- basically this specifies who is able to access the files and who own it.

	3.Encrypiton

	4.Compression: implemented on a per file or per directory basis.

	5.Unicode Character set.


In NTFS everything is considered to be a file

	1. the volume administrative data is held in system files along with normal data file contents

	2.the administrative data can be anywhere physically in the volume and therefore the volume
	has no specific layout.	

	3.the only exception is the boot sector at the start.

Master File Table:
	1.It is the primary entity on a NTFS volume
	
	2.the MFT is itself a file
	
	3.if a file is small enough (including system files) it will be resident within its MFT 
	record entry and thats it.

NTFS does not use any layout, Everything can reside anywhere in the file system volume. 
it does however need the boot sector at the start of the volume as mentioned before.
it creates two two copies of the MFT which can reside anywhere. Ofcourse their location needs to be known.

the cluster size of NTFS depends on its volume size. 

Volume size		NTFS Cluster Size
7 MB - 512 MB		512 bytes
513 MB - 1024 MB	1KB
1025 MB - 2 GB		2KB
2 GB -  2 TB 		4KB

The MFT starts with a fixed size but grows as new records are required. 
NTFS usually reserves 12.5% of the volume by default for the 
exclusive use of the MFT. its location is available on PBT
(Partition Boot Record). 
MFT is basically a squence of entry records. where each record is numbered 
form the strt of the MFT, from 0.


MFT attributes: 
these may vary in types and size and not all might exist for each file.
	Standard Information: this is mandatory.
		access attributes: Read-Only etc..
		time stamps
		link count
	file name: this is mandatory.
		may be more than one
	
	security descriptor: this is Mandatory
		ACL
	
	data: this is mandatory.
		holding stream, data contents or metadata needed to access the date.

	index root: used to implement folders

	volume information: version and name of the volume

	bitmap:	used in MFT file only to locate free MFT records.

The first 16 records in MFT are from the METADATA of the volume.

NON-RESIDENT FILES
	these are larger streams which cannot be stored in MFT.

Alternate Data Streams
	in NTFS, a second data stream can be added to a file record.
	it becomes an additional file attribute.

	this allows for data to be appended to existing files.
	can be used for hiding data.

	you can only tell whether a file has adata stream attached by examining that file MFT entry.

File allocation
	assume file contents are too big and are non-resident; new file to go in root dir.

	1. Read boot sector to determine cluster size and locaiton of MFT
	2. Use MFT system file (bitmap) to find free MFT record
	3. clear contents of MFT record and initilise for file type in hand with correct attributes.
	4. use cluster bitmap file to search for file area clustes and updare data attributes with run list.
	5. Find record in MFT for root directory and update accordingly.

File Deletion
	this is the reverse of the previous process

	straight after deletion: 
		the clusters containing data have not been overwritten
		the MFT record for the file still exists
		only the MFT bitmap attribute, the cluster bitmap and the directory entry have been updated. 
